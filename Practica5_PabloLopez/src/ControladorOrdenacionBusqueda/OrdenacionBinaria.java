/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ControladorOrdenacionBusqueda;

import Listas.ListaSimple;
import modelo.Numero.Numero;

/**
 *
 * @author Usuario
 */
public class OrdenacionBinaria {
    private ListaSimple<Numero> lista;
    private ListaSimple<Numero> listaBusqueda;
    private int contador;

    public int getContador() {
        return contador;
    }

    public ListaSimple<Numero> getLista() {
        return lista;
    }

    public ListaSimple<Numero> getListaBusqueda() {
        return listaBusqueda;
    }

    public void refrescar() {
        lista = listaBusqueda;
    }

    public void llenarLista() {
        lista = new ListaSimple<>();
        for (int i = 0; i < 1000; i++) {
            Numero num = new Numero();
            num.setId(i + 1);
            num.setNumero((int) (Math.random() * 1000));
            lista.insertarDato(num);
        }
        listaBusqueda = lista;
    }

    public void ordenar() {
        contador = 0;
        lista = listaBusqueda;
        Numero[] listaAux = new Numero[lista.tamanio()];
        for (int i = 0; i < listaAux.length; i++) {
            Numero num = new Numero();
            num.setId(lista.consultar(i).getId());
            num.setNumero(lista.consultar(i).getNumero());
            listaAux[i] = num;
        }
        Numero[] aux = qsortClase(listaAux, 0, lista.tamanio() - 1);
        for (int i = 0; i < aux.length; i++) {
            lista.actualizarDato(i, aux[i]);
        }
    }

    private Numero[] qsortClase(Numero[] a, int inferior, int superior) {
        Numero[] array = a;
        int i = inferior;
        int j = superior;
        Integer x = array[Math.round((inferior + superior) / 2)].getNumero();
        while (i <= j) {
            contador++;
            while (array[i].getNumero() < x) {
                contador++;
                i++;
            }
            while (array[j].getNumero() > x) {
                contador++;
                j--;
            }
            if (i <= j) {
                Numero aux = array[i];
                array[i] = array[j];
                array[j] = aux;
                i++;
                j--;
            }
        }
        if (inferior < j) {
            qsortClase(array, inferior, j);
        }
        if (i < superior) {
            qsortClase(array, i, superior);
        }
        return array;
    }

    public boolean metodoBuscar(Integer dato) {
        ordenar();
        Integer pos = busquedaBinaria(lista, dato, 0, lista.tamanio());
        if (pos != -1) {
            ListaSimple<Numero> list = new ListaSimple<>();
            list.insertarDato(lista.consultar(pos));
            lista = list;
            return true;
        } else {
            return false;
        }
    }

    public int busquedaBinaria(ListaSimple<Numero> lista, Integer dato, int inicio, int fin) {
        int centro = (inicio + fin) / 2;
        if (fin < inicio) {
            return -1;
        }
        if (dato < lista.consultar(centro).getNumero()) {
            return busquedaBinaria(lista, dato, inicio, centro - 1);
        }
        if (dato > lista.consultar(centro).getNumero()) {
            return busquedaBinaria(lista, dato, centro + 1, fin);
        }
        if (dato.equals(lista.consultar(centro).getNumero())) {
            return centro;
        }
        return -1;
    }
}
