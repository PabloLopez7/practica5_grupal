/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista.persona;

import controlador.tda.lista.ListaEnlazadaServices;
import controlador.tda.persona.PersonaController;
import controlador.utiles.TipoOrdenacion;
import javax.swing.JOptionPane;
import vista.persona.tabla.tablaPersonas;

/**
 *
 * @author Usuario
 */
public class frmPersona extends javax.swing.JDialog {

    private PersonaController personaController = new PersonaController();
    private tablaPersonas tp = new tablaPersonas();
    private int pos = -1;

    /**
     * Creates new form frmPersona
     */
    public frmPersona(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        buttonGroup1.add(radioAsc);
        buttonGroup1.add(radioDes);
        buttonGroup1.add(radioNin);
        cargarTabla();
    }

    private void cargarTabla() {
        tp.setLista(personaController.getListaPersonas());
        tbl_tabla.setModel(tp);
        tbl_tabla.updateUI();
    }

    private void Limpiar() {
        txt_apellidos.setText("");
        txt_nombres.setText("");
        txt_cedula.setText("");
        txt_fechaNacimiento.setText("");
        personaController.setPersona(null);
        pos = -1;
        cargarTabla();
        radioNin.setSelected(true);
    }
    
    public void buscar() throws Exception{
        String txt = txt_Buscar.getText();
        ListaEnlazadaServices lista= new ListaEnlazadaServices();
        if(txt.trim().isEmpty()){
            JOptionPane.showMessageDialog(null, "Agregue lo que desea buscar", "Error", JOptionPane.ERROR_MESSAGE);
            cargarTabla();
        }else{
            String atributo = "cedula";
            if(cbxBuscar.getSelectedItem().toString().equalsIgnoreCase("apellido"))
                atributo = "apellido";
            else if(cbxBuscar.getSelectedItem().toString().equalsIgnoreCase("nombres"))
                atributo = "nombres";
            lista.setLista(personaController.getListaPersonas().getLista().ordenar_seleccion("id", TipoOrdenacion.ASCENDENTE).buscar(atributo, txt));
            tp.setLista(lista);
            tbl_tabla.setModel(tp);
            tbl_tabla.updateUI();
        }
    }
    

    private void ordenar() throws Exception {
        if (radioNin.isSelected()) {
            cargarTabla();
        } else {
            String atributo = "Cedula" + "Nombres";
            atributo = (jComboBox1.getSelectedIndex() == 0) ? "Apellidos" : atributo;
            ListaEnlazadaServices lista = new ListaEnlazadaServices();
            if (radioAsc.isSelected()) {
                lista.setLista(personaController.getListaPersonas().getLista().ordenar_seleccion(atributo, TipoOrdenacion.ASCENDENTE));
            } else if (radioDes.isSelected()) {
                lista.setLista(personaController.getListaPersonas().getLista().ordenar_seleccion(atributo, TipoOrdenacion.DESCENDENTE));
            }
            tp.setLista(lista);
            tbl_tabla.setModel(tp);
            tbl_tabla.updateUI();
        }

    }

    private void guardar() {
        if (txt_apellidos.getText().trim().isEmpty() || txt_nombres.getText().trim().isEmpty()
                || txt_cedula.getText().trim().isEmpty() || txt_fechaNacimiento.getText().trim().isEmpty()) {
            JOptionPane.showMessageDialog(null, "No hay componentes", "Error", JOptionPane.ERROR_MESSAGE);
        } else {
            personaController.getPersona().setApellidos(txt_apellidos.getText().trim());
            personaController.getPersona().setNombres(txt_nombres.getText().trim());
            personaController.getPersona().setCedula(txt_cedula.getText().trim());
            personaController.getPersona().setFechaNacimiento(txt_fechaNacimiento.getText().trim());

            if (pos == -1) {
                if (personaController.getListaPersonas().insertarAlFinal(personaController.getPersona())) {
                    JOptionPane.showMessageDialog(null, "Se guardo", "OK", JOptionPane.INFORMATION_MESSAGE);
                    Limpiar();
                } else {
                    JOptionPane.showMessageDialog(null, "No se guardo", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                if (personaController.getListaPersonas().modificarDatoPosicion(pos, personaController.getPersona())) {
                    JOptionPane.showMessageDialog(null, "Se ha modificado", "OK", JOptionPane.INFORMATION_MESSAGE);
                    Limpiar();
                } else {
                    JOptionPane.showMessageDialog(null, "No se pudo modificar", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        }
    }

    private void cargarVista() {
        int fila = tbl_tabla.getSelectedRow();
        if (fila >= 0) {
            personaController.setPersona(tp.getLista().obtenerDato(fila));
            txt_apellidos.setText(personaController.getPersona().getApellidos());
            txt_nombres.setText(personaController.getPersona().getNombres());
            txt_cedula.setText(personaController.getPersona().getCedula());
            txt_fechaNacimiento.setText(personaController.getPersona().getFechaNacimiento());
            pos = fila;
        } else {
            JOptionPane.showMessageDialog(null, "Elija un comando de la tabla", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_apellidos = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_nombres = new javax.swing.JTextField();
        txt_cedula = new javax.swing.JTextField();
        txt_fechaNacimiento = new javax.swing.JTextField();
        btn_guardar = new javax.swing.JButton();
        btn_cancelar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_tabla = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        cbxBuscar = new javax.swing.JComboBox();
        txt_Buscar = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jComboBox1 = new javax.swing.JComboBox();
        radioAsc = new javax.swing.JRadioButton();
        radioDes = new javax.swing.JRadioButton();
        jComboBox2 = new javax.swing.JComboBox();
        radioNin = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Personas");
        getContentPane().setLayout(null);

        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel1.setLayout(null);

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("AdministrarPersonas"));
        jPanel2.setLayout(null);

        jLabel1.setText("Apellidos :");
        jPanel2.add(jLabel1);
        jLabel1.setBounds(20, 30, 70, 14);
        jPanel2.add(txt_apellidos);
        txt_apellidos.setBounds(130, 20, 230, 30);

        jLabel2.setText("Nombres: ");
        jPanel2.add(jLabel2);
        jLabel2.setBounds(20, 70, 60, 14);

        jLabel3.setText("Cedula: ");
        jPanel2.add(jLabel3);
        jLabel3.setBounds(20, 110, 50, 14);

        jLabel4.setText("Fecha Nacimiento: ");
        jPanel2.add(jLabel4);
        jLabel4.setBounds(20, 150, 100, 14);
        jPanel2.add(txt_nombres);
        txt_nombres.setBounds(130, 60, 230, 30);
        jPanel2.add(txt_cedula);
        txt_cedula.setBounds(130, 100, 230, 30);
        jPanel2.add(txt_fechaNacimiento);
        txt_fechaNacimiento.setBounds(130, 140, 230, 30);

        btn_guardar.setText("Guardar");
        btn_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarActionPerformed(evt);
            }
        });
        jPanel2.add(btn_guardar);
        btn_guardar.setBounds(40, 180, 90, 23);

        btn_cancelar.setText("Cancelar");
        jPanel2.add(btn_cancelar);
        btn_cancelar.setBounds(190, 180, 120, 23);

        jPanel1.add(jPanel2);
        jPanel2.setBounds(10, 10, 390, 220);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Listado de Personas"));
        jPanel3.setLayout(null);

        tbl_tabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbl_tabla);

        jPanel3.add(jScrollPane1);
        jScrollPane1.setBounds(10, 70, 650, 80);

        jButton1.setText("Editar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton1);
        jButton1.setBounds(20, 160, 63, 23);

        jLabel6.setText("Buscar: ");
        jPanel3.add(jLabel6);
        jLabel6.setBounds(20, 30, 60, 14);

        cbxBuscar.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Nombre", "Apellido", "Cedula" }));
        jPanel3.add(cbxBuscar);
        cbxBuscar.setBounds(100, 30, 100, 22);
        jPanel3.add(txt_Buscar);
        txt_Buscar.setBounds(230, 20, 200, 30);

        jButton2.setText("Buscar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton2);
        jButton2.setBounds(450, 20, 120, 23);

        jPanel1.add(jPanel3);
        jPanel3.setBounds(10, 240, 680, 200);

        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder("Metodo de Ordenamiento"));
        jPanel4.setLayout(null);

        jLabel5.setText("Ordenar:");
        jPanel4.add(jLabel5);
        jLabel5.setBounds(20, 20, 60, 14);

        jComboBox1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Metodo Shell", "Metodo Quicksort" }));
        jComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox1ActionPerformed(evt);
            }
        });
        jPanel4.add(jComboBox1);
        jComboBox1.setBounds(20, 50, 210, 22);

        radioAsc.setText("Ascendente");
        radioAsc.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radioAscItemStateChanged(evt);
            }
        });
        jPanel4.add(radioAsc);
        radioAsc.setBounds(20, 120, 93, 23);

        radioDes.setText("Descendente");
        radioDes.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radioDesItemStateChanged(evt);
            }
        });
        jPanel4.add(radioDes);
        radioDes.setBounds(140, 120, 89, 23);

        jComboBox2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Nombres", "Apellidos", "Cedula" }));
        jComboBox2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                jComboBox2ItemStateChanged(evt);
            }
        });
        jPanel4.add(jComboBox2);
        jComboBox2.setBounds(20, 90, 210, 22);

        radioNin.setText("Ninguno");
        radioNin.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                radioNinItemStateChanged(evt);
            }
        });
        jPanel4.add(radioNin);
        radioNin.setBounds(70, 150, 140, 23);

        jPanel1.add(jPanel4);
        jPanel4.setBounds(420, 20, 250, 200);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 710, 450);

        setSize(new java.awt.Dimension(735, 504));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void btn_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarActionPerformed
        // TODO add your handling code here:
        guardar();
    }//GEN-LAST:event_btn_guardarActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        cargarVista();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jComboBox2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_jComboBox2ItemStateChanged
        // TODO add your handling code here:
        try {
            ordenar();
        } catch (Exception e) {
            System.out.println("Error" + e);
        }
    }//GEN-LAST:event_jComboBox2ItemStateChanged

    private void radioAscItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radioAscItemStateChanged
        // TODO add your handling code here:
        try {
            ordenar();
        } catch (Exception e) {
            System.out.println("Error" + e);
        }
    }//GEN-LAST:event_radioAscItemStateChanged

    private void radioDesItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radioDesItemStateChanged
        // TODO add your handling code here:
        try {
            ordenar();
        } catch (Exception e) {
            System.out.println("Error" + e);
        }
    }//GEN-LAST:event_radioDesItemStateChanged

    private void radioNinItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_radioNinItemStateChanged
        // TODO add your handling code here:
        try {
            ordenar();
        } catch (Exception e) {
            System.out.println("Error" + e);
        }
    }//GEN-LAST:event_radioNinItemStateChanged

    private void jComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jComboBox1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        try {
            buscar();
        } catch (Exception e) {
            System.out.println("Error" + e);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmPersona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmPersona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmPersona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmPersona.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                frmPersona dialog = new frmPersona(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancelar;
    private javax.swing.JButton btn_guardar;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox cbxBuscar;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JComboBox jComboBox1;
    private javax.swing.JComboBox jComboBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JRadioButton radioAsc;
    private javax.swing.JRadioButton radioDes;
    private javax.swing.JRadioButton radioNin;
    private javax.swing.JTable tbl_tabla;
    private javax.swing.JTextField txt_Buscar;
    private javax.swing.JTextField txt_apellidos;
    private javax.swing.JTextField txt_cedula;
    private javax.swing.JTextField txt_fechaNacimiento;
    private javax.swing.JTextField txt_nombres;
    // End of variables declaration//GEN-END:variables
}
