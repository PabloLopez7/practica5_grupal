/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Listas;

/**
 *
 * @author Usuario
 */
public class ListaSimple<T> {

    public NodoDato cabecera;

    public ListaSimple() {
        this.cabecera = null;
    }

    public boolean estaVacio() {
        return (this.cabecera == null);
    }

    public void imprimir() {
        if (!estaVacio()) {
            NodoDato tmp = cabecera;
            while (tmp != null) {
                System.out.println(tmp.getDato());
                tmp = tmp.getSiguiente();
            }
        }
    }

    private void insertar(T dato) {
        NodoDato tmp = new NodoDato(dato, cabecera);
        cabecera = tmp;
    }

    public boolean insertarDato(T dato) {
        try {
            insertarFinal(dato);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public T extraer() {
        T dato = null;
        if (!estaVacio()) {
            dato = (T) cabecera.getDato();
            cabecera = cabecera.getSiguiente();
        }
        return dato;
    }

    public T consultar(int pos) {
        T dato = null;
        if (!estaVacio() && (pos <= (tamanio() - 1))) {
            NodoDato tmp = cabecera;
            for (int i = 0; i < pos; i++) {
                tmp = tmp.getSiguiente();
                if (tmp == null) {
                    break;
                }
            }
            if (tmp != null) {
                dato = (T) tmp.getDato();
            }
        }
        return dato;
    }

    public int tamanio() {
        int tamanio = 0;
        if (!estaVacio()) {
            NodoDato tmp = cabecera;
            while (tmp != null) {
                tamanio++;
                tmp = tmp.getSiguiente();
            }
        }
        return tamanio;
    }

    public boolean actualizarDato(int pos, T dato) {
        if (!estaVacio() && (pos <= (tamanio() - 1))) {
            NodoDato tmp = cabecera;
            for (int i = 0; i < pos; i++) {
                tmp = tmp.getSiguiente();
                if (tmp == null) {
                    break;
                }
            }
            if (tmp != null) {
                tmp.setDato(dato);
                return true;
            }
        }
        return false;
    }

    public void insertar(T dato, int pos) {
        if (estaVacio() || pos < 0) {
            insertar(dato);
        } else {
            NodoDato iterador = cabecera;
            for (int i = 0; i < pos; i++) {
                if (iterador.getSiguiente() == null) {
                    break;
                }
                iterador = iterador.getSiguiente();
            }
            NodoDato tmp = new NodoDato(dato, iterador.getSiguiente());
            iterador.setSiguiente(tmp);
        }
    }

    private void insertarFinal(T dato) {
        insertar(dato, (tamanio() - 1));
    }
}
