/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador.ordenacion.arreglo;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 *
 * @author Usuario
 */
public class Ordenacion {

    private Integer matriz[];
    private static Integer SIZE = 10000;

    public Integer[] getMatriz() {
        return matriz;
    }

    public void llenarArchivo() {
        try {
            FileWriter file = new FileWriter("datos"+File.separatorChar+"numero_txt", true);
            
            for (int i = 0; i < SIZE; i++) {
                Integer aux = (int) (Math.random() * 1000);
                file.write(aux.toString() + "\n");               
            }            
            file.close();
        } catch (Exception e) {
            System.out.println("Error"+e);
        }
    }
    
    public void llenarMatriz() {
        try {
            matriz = new Integer[SIZE];
            FileReader fr = new FileReader("datos" + File.separatorChar + "numero_txt");
            BufferedReader entrada = new BufferedReader(fr);
            String aux = entrada.readLine();
            Integer cont = 0;
            while (aux != null) {
                matriz[cont] = Integer.parseInt(aux);
                aux = entrada.readLine();
                cont++;
            }
            fr.close();
            entrada.close();
        } catch (Exception e) {
        }
    }

    public void imprimir() {
        System.out.println("******************");
        for (int i = 0; i < matriz.length; i++) {
            System.out.print(matriz[i] + "\t");
        }
        System.out.println("");
        System.out.println("******************");
    }

    public void burbuja(Integer[] matriz) {
        Integer cont = 0;
        for (int i = matriz.length; i > 1; i--) {

            for (int j = 0; j < i - 1; j++) {
                if (matriz[j] > matriz[j + 1]) {
                    Integer aux = matriz[j];
                    matriz[j] = matriz[j + 1];
                    matriz[j + 1] = aux;
                }
                cont++;
            }
        }
        System.out.println("INTERCAMBIOS " + cont);
    }

    public void shakeSort(Integer[] matriz) {
        Integer cont = 0;
        Integer i, j, izq, der;
        izq = 2;
        der = matriz.length - 1;
        j = matriz.length - 1;
        do {
            for (i = der; i >= izq; i--) {
                if (matriz[i - 1] > matriz[i]) {
                    Integer aux = matriz[i];
                    matriz[i] = matriz[i - 1];
                    matriz[i - 1] = aux;
                    j = i;
                }
                cont++;
            }
            izq = j + 1;
            for (i = izq; i <= der; i++) {
                if (matriz[i - 1] > matriz[i]) {
                    Integer aux = matriz[i];
                    matriz[i] = matriz[i - 1];
                    matriz[i - 1] = aux;
                }
                cont++;
            }
            der = j - 1;
        } while (izq <= der);
        System.out.println("ITERACIONES " + cont);
    }

    public void insercion(Integer[] matriz) {
        Integer cont = 0;
        int j, aux = 0;
        for (int i = 1; i < matriz.length; i++) {
            j = i - 1;
            aux = matriz[i]; // 4   7   9   1  2
            while (j >= 0 && aux < matriz[j]) {
                matriz[j + 1] = matriz[j];
                j = j - 1;
                cont++;
            }
            matriz[j + 1] = aux;
        }
        System.out.println("INTERCAMBIOS " + cont);
    }

    public void seleccion(Integer[] matriz) {
        Integer i, j, k, t = 0;
        Integer n = matriz.length;
        Integer cont = 0;
        for (i = 0; i < n - 1; i++) {
            k = i;
            t = matriz[i];
            for (j = i + 1; j < n; j++) {
                if (matriz[j] < t) {
                    t = matriz[j];//
                    k = j;//intercambio
                    cont++;
                }
            }
            matriz[k] = matriz[i];//intercambias cuando encountra el valor
            matriz[i] = t;
            cont++;
        }
        
        System.out.println("INTERCAMBIOS " + cont);
    }
    
    public void shell(Integer [] matriz){
        Integer cont = 0;
        Integer salto, i,j,k,aux;
        salto=matriz.length/2;
        while(salto>0){
            for(i=salto;i<matriz.length;i++){
                j=i-salto;
                while(j>=0){
                    k=j+salto;
                    if(matriz[j]<=matriz[k]){
                        j=-1;
                        cont++;
                    }else{
                        aux=matriz[j];
                        matriz[j]=matriz[k];
                        matriz[k]=aux;
                        j-=salto;
                        cont++;
                    }
                }
            }
            salto=salto/2;
            
        }
        System.out.println("INTERCAMBIOS " + cont);
        //mostraMatriz(matriz);
    }
    
    public static Integer[] quicksort(Integer A[], int izq, int der) {

        int pivote = A[izq]; // tomamos primer elemento como pivote
        int i = izq;         // i realiza la búsqueda de izquierda a derecha
        int j = der;         // j realiza la búsqueda de derecha a izquierda
        int aux;

        while (i < j) {                          // mientras no se crucen las búsquedas                                   
            while (A[i] <= pivote && i < j) {
                i++; // busca elemento mayor que pivote
            }
            while (A[j] > pivote) {
                j--;           // busca elemento menor que pivote
            }
            if (i < j) {                        // si no se han cruzado                      
                aux = A[i];                      // los intercambia
                A[i] = A[j];
                A[j] = aux;
            }
        }

        A[izq] = A[j];      // se coloca el pivote en su lugar de forma que tendremos                                    
        A[j] = pivote;      // los menores a su izquierda y los mayores a su derecha

        if (izq < j - 1) {
            A=quicksort(A, izq, j - 1);          // ordenamos subarray izquierdo
        }
        if (j + 1 < der) {
            A=quicksort(A, j + 1, der);          // ordenamos subarray derecho
        }
        return A;
    }
    
    public static void main(String[] args) {
        Ordenacion o = new Ordenacion();

        //LLAMADO METODO BURBUJA
        //ITERACIONES 49995000   49995000
        //Tiempo0.0
        
        //LLAMADO METODO BURBUJA MEJORADA
        //ITERACIONES 19995       19995
        //Tiempo0.0
        
        //LLAMADO METODO DE INSERCION
        //INTERCAMBIOS 25054688
        //Tiempo0.0
        
        //LLAMADO METODO DE SELECCION
        //INTERCAMBIOS 73343
        //Tiempo0.0
        
        //LLAMADO METODO DE SHELL
        //INTERCAMBIOS 254748
        //Matriz Ordenada con shell
        //Tiempo0.0
        
        //LLAMADO METODO DE QUICKSHORT
        //INTERCAMBIOS 9980
        //Tiempo0.0
        
        o.llenarArchivo();
        o.llenarMatriz();
        o.imprimir();
        long inicio = System.currentTimeMillis();
        //System.out.println("LLAMADO METODO BURBUJA");
        //System.out.println("LLAMADO METODO BURBUJA MEJORADA");
        //System.out.println("LLAMADO METODO DE INSERCION");
        //System.out.println("LLAMADO METODO DE SELECCION");
        //System.out.println("LLAMADO METODO DE SHELL");
        System.out.println("LLAMADO METODO DE QUICKSHORT");
//        o.burbuja(o.getMatriz());
//        long fin = System.currentTimeMillis();
//        double tiempo = (double) ((fin - inicio) / 1000);
//        System.out.println("Tiempo" + tiempo);
//        o.imprimir();
//        
//        o.shakeSort(o.getMatriz());
//        long fin = System.currentTimeMillis();
//        double tiempo = (double) ((fin - inicio) / 1000);
//        System.out.println("Tiempo" + tiempo);
//        o.imprimir();
        
//        o.insercion(o.getMatriz());
//        long fin = System.currentTimeMillis();
//        double tiempo = (double) ((fin - inicio) / 1000);
//        System.out.println("Tiempo" + tiempo);
//        o.imprimir();
        
//        o.seleccion(o.getMatriz());
//        long fin = System.currentTimeMillis();
//        double tiempo = (double) ((fin - inicio) / 1000);
//        System.out.println("Tiempo" + tiempo);
//        o.imprimir();
        
//        o.shell(o.getMatriz());
//        long fin = System.currentTimeMillis();
//        double tiempo = (double) ((fin - inicio) / 1000);
//        System.out.println("Tiempo" + tiempo);
//        o.imprimir();
        
        System.out.println(o.getMatriz().length);
        Integer [] salida = o.quicksort(o.getMatriz(), 0, ((int)(o.getMatriz().length/2)));
        long fin = System.currentTimeMillis();
        double tiempo = (double) ((fin - inicio) / 1000);
        System.out.println("Tiempo" + tiempo);
        //o.imprimir();
        System.out.println("*******************");
        for(int i=0; i<salida.length; i++){
            System.out.println(salida[i] + "\t");
        }
        System.out.println("*******************");
    }

}
