/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package vista.numero.tabla;

import Listas.ListaSimple;
import javax.swing.table.AbstractTableModel;
import modelo.Numero.Numero;

/**
 *
 * @author Usuario
 */
public class ModeloTablaNumeros extends AbstractTableModel{
    private ListaSimple<Numero> lista;

    public ListaSimple<Numero> getLista() {
        return lista;
    }

    public void setLista(ListaSimple<Numero> lista) {
        this.lista = lista;
    }
    
    @Override
    public int getRowCount() {
        return lista.tamanio();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int i, int i1) {
        Numero dato = lista.consultar(i);
        switch(i1) {
            case 0: return (dato != null) ? dato.getId(): ""; 
            case 1: return (dato != null) ? dato.getNumero(): "";    
            default: return null;
        }
    }
    
    @Override
    public String getColumnName(int i) {
        switch(i) {
            case 0: return "ID";
            case 1: return "Número";   
            default: return null;
        }
    }
}
