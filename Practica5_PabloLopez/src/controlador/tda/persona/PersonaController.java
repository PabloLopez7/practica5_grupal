/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package controlador.tda.persona;

import controlador.tda.lista.ListaEnlazadaServices;
import modelo.Persona.Persona;

/**
 *
 * @author Usuario
 */
public class PersonaController {
    private Persona persona;
    private ListaEnlazadaServices<Persona> listaPersonas;

    public ListaEnlazadaServices<Persona> getListaPersonas() {
        if(this.listaPersonas == null)
            this.listaPersonas = new ListaEnlazadaServices<>();
        return listaPersonas;
    }

    public void setListaPersonas(ListaEnlazadaServices<Persona> listaPersonas) {
        this.listaPersonas = listaPersonas;
    }

    public Persona getPersona() {
        if(this.persona == null)
            this.persona= new Persona();
        return persona;
    }

    public void setPersona(Persona persona) {
        this.persona = persona;
    }
    
    
}
